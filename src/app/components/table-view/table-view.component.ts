import { Component, OnInit } from '@angular/core';

import { DataService } from './../../service/data.service';

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.scss']
})
export class TableViewComponent implements OnInit {

tableData: any
display: any = false

  constructor(private dataService: DataService) { }

  ngOnInit(): void {

    this.dataService.getData().subscribe((res) => {
      this.tableData = res
    })
  }

  

}
