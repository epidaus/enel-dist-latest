import { Component, OnInit } from '@angular/core';

import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-potree-viewer',
  templateUrl: './potree-viewer.component.html',
  styleUrls: ['./potree-viewer.component.scss']
})
export class PotreeViewerComponent implements OnInit {

  datas: any
  // files: any

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    
    this.dataService.onSharedTree.subscribe((data) => {
      this.datas = data
    })

    if(this.dataService.ClosePVar==undefined){
      this.dataService.ClosePVar = this.dataService.invokePotreeCloseFunction.subscribe(()=>{
        this.close()
      })
    }
    
  }

  close(){
    this.datas = undefined
    this.dataService.onSmallCardCloseClick()
    this.dataService.selectedTree = this.datas
  }

  // eventEmitted(event) {
  //   console.log(event);
  //   // this.router.navigate([`project/${event.value.row.id}/executive_summary/`])
  // }

}
