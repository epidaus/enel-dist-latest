// import { DataService } from 'src/app/service/data.service';
import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';

@Component({
  selector: 'app-report-algarrobal',
  templateUrl: './report-algarrobal.component.html',
  styleUrls: ['./report-algarrobal.component.scss']
})
export class ReportTreeComponent implements OnInit {
  @Input() treeParent: any
  @Input() siteParent: any

  sites: any
  status: any

  constructor(private data: DataService) { }

  ngOnInit(): void {
    this.data.getData().subscribe((res => {
      this.sites = res.filter(item => item.site_id === 2)[0]
    }))
  }

  getClass(val) {
    if (val >= 1 && val <= 10) {
      return 'Bueno'
    } else if (val >= 11 && val <= 20) {
      return 'Leve'
    } else if (val >= 21 && val <= 46) {
      return 'Critico'
    } else {
      return 'none'
    }
  }

}
