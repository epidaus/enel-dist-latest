import { ExportToCsv } from 'export-to-csv';
import { DataService } from './../../service/data.service';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-card-icon',
  templateUrl: './card-icon.component.html',
  styleUrls: ['./card-icon.component.scss'],
})
export class CardIconComponent implements OnInit {
  showIcon = false;
  selectedModal: string;
  datas: any;
  siteName: any;
  files: any;
  photo_path: any;
  modalRef: BsModalRef;
  csvDatas = [];
  searchText = '';
  items: any;
  uniques: any;
  statusAlto: string = 'active';
  statusMedio: string = 'active';
  statusBajo: string = 'active';
  statusConAlto: string = 'active';
  statusConMedio: string = 'active';
  statusConBajo: string = 'active';
  constructor(
    private dataService: DataService,
    private modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.dataService.onSharedId.subscribe((data) => {
      this.datas = data;
      this.siteName = data.site_name;
      this.items = data.file_name;
      this.uniques = [
        ...new Set(this.items.map((item) => item.scientific_name)),
      ];
    });
    this.dataService.onSharedFile.subscribe((data) => {
      this.files = data;
      // console.log("icon",this.files);
    });

    if (this.dataService.CloseLogoIconVar == undefined) {
      this.dataService.CloseLogoIconVar = this.dataService.invokeLogoCloseIconFunction.subscribe(
        () => {
          this.close();
          this.searchText = '';
        }
      );
    }
  }

  openModalGallery(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.setPhoto(this.files.photo_path);
  }

  openPotree() {
    this.dataService.selectedTree = this.dataService.activeFile;
  }

  close() {
    this.files = undefined;
    this.datas = undefined;
  }

  downloadCsv() {
    this.datas.file_name.map((data) => {
      var csvData = {
        tree_id: data.name,
        latitude: data.lat,
        longitude: data.lng,
        distance_to_line: data.distance_to_line,
        tree_height: data.height,
        severity: data.severity,
        avenue_name: data.avenue_name,
        scientific_name: data.scientific_name,
        common_name: data.common_name,
        height: data.height,
        percentage_of_tree_fall: data.percentage_of_tree_fall,
        impact: data.impact,
        magnitude: data.magnitude,
        rma33_critically: data.rma33_critically,
        rma34_critically: data.rma34_critically,
        tree_health_status: data.tree_health_status,
        postal_address: data.postal_address,
        estate: data.estate,
        close_inspection_lidar: data.close_inspection_lidar,
        tension_level: data.tension_level,
        network_type: data.network_type,
        numpos: data.numpos,
        common: data.common,
        magnitude_of_risk: data.magnitude_of_risk,
        risk_classification_wo_health: data.risk_classification_wo_health,
        magnitude_with_health: data.magnitude_with_health,
        risk_classification_w_health: data.risk_classification_w_health,
      };
      this.csvDatas.push(csvData);
    });
    // console.log(this.csvDatas)

    const options = {
      filename: this.datas.site_name,
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: true,
      title: this.datas.site_name,
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
    };

    const csvExporter = new ExportToCsv(options);

    csvExporter.generateCsv(this.csvDatas);
  }

  setPhoto(photo) {
    this.photo_path = photo;
  }

  onFilterClick(item) {
    this.searchText = item;
    this.dataService.onFilterSearchClick();
    this.dataService.onFilterMapClick(item);
  }

  onClearTextClick() {
    this.searchText = '';
    this.dataService.onLegendResetMapClick();
    this.dataService.onFilterSearchClick();
  }

  onSearchIconClick(searchText) {
    this.dataService.onFilterSearchClick();
    this.dataService.onFilterMapClick(searchText);
  }

  onClassificationClick(type: string) {
    switch (type) {
      case 'sinAlto':
        if (this.statusAlto == 'active') {
          this.statusAlto = 'inactive';
        } else if (this.statusAlto == 'inactive') {
          this.statusAlto = 'active';
        }
        this.dataService.onFilterClassificationClick(this.statusAlto, type);
        break;
      case 'sinMedio':
        if (this.statusMedio == 'active') {
          this.statusMedio = 'inactive';
        } else if (this.statusMedio == 'inactive') {
          this.statusMedio = 'active';
        }
        this.dataService.onFilterClassificationClick(this.statusMedio, type);
        break;
      case 'sinBajo':
        if (this.statusBajo == 'active') {
          this.statusBajo = 'inactive';
        } else if (this.statusBajo == 'inactive') {
          this.statusBajo = 'active';
        }
        this.dataService.onFilterClassificationClick(this.statusBajo, type);
        break;
        case 'conAlto':
        if (this.statusConAlto == 'active') {
          this.statusConAlto = 'inactive';
        } else if (this.statusConAlto == 'inactive') {
          this.statusConAlto = 'active';
        }
        this.dataService.onFilterClassificationClick(this.statusConAlto, type);
        break;
      case 'conMedio':
        if (this.statusConMedio == 'active') {
          this.statusConMedio = 'inactive';
        } else if (this.statusConMedio == 'inactive') {
          this.statusConMedio = 'active';
        }
        this.dataService.onFilterClassificationClick(this.statusConMedio, type);
        break;
      case 'conBajo':
        if (this.statusConBajo == 'active') {
          this.statusConBajo = 'inactive';
        } else if (this.statusConBajo == 'inactive') {
          this.statusConBajo = 'active';
        }
        this.dataService.onFilterClassificationClick(this.statusConBajo, type);
        break;
    }
  }

  openAnalysis() {

  }
}
