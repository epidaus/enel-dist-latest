import { DataService } from 'src/app/service/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss'],
})
export class LogoComponent implements OnInit {
  constructor(private data: DataService) {}
  datas: any;
  statusGood: string = 'active';
  statusMinor: string = 'active';
  statusCritical: string = 'active';
  ngOnInit(): void {
    this.data.onSharedId.subscribe((data) => {
      this.datas = data;
    });

    if (this.data.LegendVar == undefined) {
      this.data.LegendVar = this.data.invokeLegendResetFunction.subscribe(
        () => {
          this.statusGood = this.statusMinor = this.statusCritical = undefined;
        }
      );
    }
  }

  home() {
    this.data.onLogoCloseAnalysisClick()
    this.data.onResetMapClick();
    this.data.onLogoCloseIconClick();
    this.data.onLogoClosePCardClick();
    this.data.onPotreeCloseClick();
    this.data.onHeaderTitleCloseClick();
    this.statusGood = this.statusMinor = this.statusCritical = undefined;
    this.datas = undefined;
  }

  onLegendClick(severity: string) {
    if (this.datas) {
      switch (severity) {
        case 'Good':
          if (this.statusGood == 'active' || this.statusGood == undefined) {
            this.statusGood = 'inactive';
            this.data.onFilterClick(this.statusGood, severity);
          } else {
            this.statusGood = 'active';
            this.data.onFilterClick(this.statusGood, severity);
          }
          break;
        case 'Minor':
          if (this.statusMinor == 'active' || this.statusMinor == undefined) {
            this.statusMinor = 'inactive';
            this.data.onFilterClick(this.statusMinor, severity);
          } else {
            this.statusMinor = 'active';
            this.data.onFilterClick(this.statusMinor, severity);
          }
          break;
        case 'Critical':
          if (this.statusCritical == 'active' || this.statusCritical == undefined) {
            this.statusCritical = 'inactive';
            this.data.onFilterClick(this.statusCritical, severity);
          } else {
            this.statusCritical = 'active';
            this.data.onFilterClick(this.statusCritical, severity);
          }
          break;
      }
    }
  }

  onResetClick() {
    if (this.datas) {
      this.data.onLegendResetClick();
      this.statusGood = this.statusMinor = this.statusCritical = undefined;
    }
  }
}
