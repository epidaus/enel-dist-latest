import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { SpeedTestService } from 'ng-speed-test';

@Component({
	selector: 'app-internet-speed-monitor',
	templateUrl: './internet-speed-monitor.component.html',
	styleUrls: ['./internet-speed-monitor.component.scss']
})
export class InternetSpeedMonitorComponent implements OnInit, AfterViewInit, OnDestroy {

	currentSpeed:number = 100;

	testInterval:number;
	testSubscription:Subscription;
	testIsActive:boolean = false;

	constructor(private speedTestService: SpeedTestService) { }

	ngOnInit(): void {

	}

	ngAfterViewInit () :void {
		this.testInterval = setInterval(() => {
			this.startTest();
		}, 5000);
	}

	startTest () {

		if (this.testSubscription) {
			this.testSubscription.unsubscribe();
		}

		this.testIsActive = true;

		this.testSubscription = this.speedTestService.getMbps({
			iterations: 10,
			retryDelay: 500,
		}).subscribe(speed => {
			// console.log('Your speed is ' + speed);
			let rounded = Math.round((speed + Number.EPSILON) * 100) / 100;
			this.currentSpeed = rounded;
		});

	}

	ngOnDestroy () :void {
		if (this.testSubscription) {
			this.testSubscription.unsubscribe();
		}

		this.testIsActive = false;
	}

	getSpeedColor () :string {
		if (this.currentSpeed > 50) {
			return "green";
		} else if (this.currentSpeed > 10) {
			return "yellow";
		} else {
			return "red";
		}
	}

}
