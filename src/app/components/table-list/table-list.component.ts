import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Columns, Config, DefaultConfig } from 'ngx-easy-table';
import { DataService } from './../../service/data.service';

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.scss']
})
export class TableListComponent implements OnInit {
  temp = []
  rows = []
  projectID: any
  dataList: any
  public configuration: Config;
  public columns1: Columns[];
  public columns2: Columns[];

  public data: any

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private activatedRoute: ActivatedRoute, private dataService: DataService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(({id}) => {
      this.projectID = id
      this.dataList = this.dataService.getDAtabyId(id)
      
      // this.data = this.dataList.file_name
      this.rows = this.dataList.file_name

      this.temp = [...this.dataList.file_name];
    })


    
    

    this.configuration = { ...DefaultConfig };
    this.configuration.searchEnabled = true;

    
    this.columns1 = [
      { key: 'name', title: 'ID árbol' },
      { key: 'lat', title: 'Latitud' },
      { key: 'lng', title: 'longitud' },
      { key: 'avenue_name', title: 'Nombre Avenida' },
      { key: 'scientific_name', title: 'Nombre científico' },
      { key: 'class', title: 'Clase' },
      { key: 'family', title: 'Familia' },
      { key: 'common_name', title: 'Nombre común' },
      { key: '', title: 'Distancia a la línea (m)' },
      { key: '', title: 'Altura (m)' },
      { key: '', title: 'Mayor severidad' },
      { key: '', title: 'Porcentaje de árbol proyectado a caer' },
      { key: '', title: 'Impacto' },
      { key: '', title: 'Magnitud' },
      { key: '', title: 'Criticidad RMA33' },
      { key: '', title: 'Criticidad RMA3' },
      { key: '', title: 'Estado de salud del árbol' },
      { key: '', title: 'Dirección postal' },
      { key: '', title: 'Predio' },
      { key: '', title: 'Fecha inspección LiDAR' },
      { key: '', title: 'Nivel de Tensión' },
      { key: '', title: 'Tipo de red' },
      { key: '', title: 'NUMPOS Equipo cercano' },
      { key: '', title: 'Comuna' },
      { key: '', title: 'Magnitud de Riesgo' },
      { key: '', title: 'Clasificación de Riesgo (sin Sanidad)' },
      { key: '', title: 'Magnitud con Sanidad' },
      { key: '', title: 'Clasificación de Riesgo (con efectoSanidad)' }
    ];

    this.columns2 = [
      { key: 'phone', title: 'ID árbol' },
      { key: 'age', title: 'Latitud' },
      { key: 'company', title: 'longitud' },
      { key: 'name', title: 'Nombre Avenida' },
      { key: 'isActive', title: 'DESC_FASE/Conductor' },
      { key: '', title: 'Nombre científico' },
      { key: '', title: 'Clase' },
      { key: '', title: 'Familia' },
      { key: '', title: 'Nombre común' },
      { key: '', title: 'Distancia a la línea (m)' },
      { key: '', title: 'Altura (m)' },
      { key: '', title: 'Mayor severidad' },
      { key: '', title: 'Porcentaje de árbol proyectado a caer' },
      { key: '', title: 'Impacto' },
      { key: '', title: 'Magnitud' },
      { key: '', title: 'Estado de salud del árbol' },
      { key: '', title: 'proceso' },
      { key: '', title: 'NUMPOS/ Equipo cercano' },
      { key: '', title: 'Equipo' },
      { key: '', title: 'Distancia (m)' },
      { key: '', title: 'Dirección postal' },
      { key: '', title: 'Comuna' },
      { key: '', title: 'Magnitud de Riesgo' },
      { key: '', title: 'Clasificación de Riesgo (sin Sanidad)' },
      { key: '', title: 'Clasificación de Riesgo C /sanidad' },
      { key: '', title: 'Clasificación de Riesgo (con efectoSanidad)' },
    ];
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
}
