import { DataService } from 'src/app/service/data.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-card-medium',
  templateUrl: './card-medium.component.html',
  styleUrls: ['./card-medium.component.scss']
})
export class CardMediumComponent implements OnInit {
  datas: any
  trees: any
  projectID: any
  @Output() public showPoetree: EventEmitter<any> = new EventEmitter()

  constructor(private data:DataService) { 
    if(this.data.HeaderVar==undefined){
      this.data.HeaderVar = this.data.invokeHeaderCloseClickFunction.subscribe(()=>{
        // console.log("logo Clicked")
        this.close()
      })
    }
  }

  ngOnInit(): void {
    this.data.onSharedId.subscribe((data)=>{
      this.datas = data
    })

    this.data.onSharedTree.subscribe((data)=>{
      this.trees = data
    })
  }

  close() {
    this.datas = undefined
  }



}
