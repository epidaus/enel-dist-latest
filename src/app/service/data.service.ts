import { Injectable, EventEmitter } from '@angular/core';
import { Observable, of, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import User from '../mock/user.mock.json';
import Line from '../mock/line.mock.json';
import Data from '../mock/data.mock.json';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  invokeLogoCloseAnalysis:EventEmitter<any> = new EventEmitter();
  LogoToAnalysisVar: Subscription;

  invokeFilterSeverityFunction:EventEmitter<any> = new EventEmitter();
  FilterSeverityVar: Subscription;

  invokeFilterClassificationFunction:EventEmitter<any> = new EventEmitter();
  FilterClassVar: Subscription;

  invokeFilterResetMapFunction:EventEmitter<any> = new EventEmitter();
  FilterResetMapVar: Subscription;

  invokeLegendResetFunction:EventEmitter<any> = new EventEmitter();
  LegendVar: Subscription;
  
  invokeResetClickFunction:EventEmitter<any> = new EventEmitter();
  ResetVar: Subscription;

  invokeFilterMapFunction: EventEmitter<any> = new EventEmitter();
  FilterVar: Subscription;

  invokePCardFunction: EventEmitter<any> = new EventEmitter();
  MarkerVar: Subscription;

  invokeHeaderCloseClickFunction: EventEmitter<any> = new EventEmitter();
  HeaderVar: Subscription;

  invokeLogoCloseIconFunction: EventEmitter<any> = new EventEmitter();
  CloseLogoIconVar: Subscription;

  invokeLogoClosePrimaryCardFunction: EventEmitter<any> = new EventEmitter();
  CloseLogoPCardVar: Subscription;
  // invokeLogoCloseFunction: EventEmitter<any> = new EventEmitter();
  // CloseLogo: Subscription;

  invokeResetMapFunction: EventEmitter<any> = new EventEmitter();
  MapVar: Subscription;

  invokeSmallCardCloseFunction: EventEmitter<any> = new EventEmitter();
  CloseSCVar: Subscription;

  // invokeGalleryIconCloseFunction: EventEmitter<any> = new EventEmitter();
  // CloseGVAr: Subscription;
  
  invokePotreeCloseFunction: EventEmitter<any> = new EventEmitter();
  ClosePVar: Subscription;

  public invokeSmallCardTreeFunction: EventEmitter<any> = new EventEmitter();
  TreeVar: Subscription;

  onSharedId: EventEmitter<any> = new EventEmitter();
  onSharedFile: EventEmitter<any> = new EventEmitter();
  onSharedTree: EventEmitter<any> = new EventEmitter();
  onSharedTable: EventEmitter<any> = new EventEmitter();
  // token = 'fake-jwt-tokeeen'
  activeData: any
  activeFile: any
  activeTree: any
  activeTable: any = false

  constructor() {}
  onLogoCloseAnalysisClick(){
    this.invokeLogoCloseAnalysis.emit()
  }

  onFilterClick(status: any, severity: string) {
    this.invokeFilterSeverityFunction.emit({status, severity})
  }

  onFilterClassificationClick(status: any, type:string) {
    this.invokeFilterClassificationFunction.emit({status, type})
  }

  onLegendResetMapClick(){
    this.invokeFilterResetMapFunction.emit()
  }

  onFilterSearchClick(){
    this.invokeLegendResetFunction.emit()
  }

  onLegendResetClick(){
    this.invokeResetClickFunction.emit()
  }

  onFilterMapClick(searchText){
    this.invokeFilterMapFunction.emit(searchText)
  }

  onPCardFunctionClick(data){
    this.invokePCardFunction.emit(data)
  }
  // onLogoCloseClick(){
  //   this.invokeLogoCloseFunction.emit()
  // }
  onHeaderTitleCloseClick(){
    this.invokeHeaderCloseClickFunction.emit()
  }
  onLogoCloseIconClick(){
    this.invokeLogoCloseIconFunction.emit()
  }

  onLogoClosePCardClick(){
    this.invokeLogoClosePrimaryCardFunction.emit()
  }

  onResetMapClick(){
    this.invokeResetMapFunction.emit()
  }

  onSmallCardCloseClick(){
    this.invokeSmallCardCloseFunction.emit()
  }

  onSmallCardTreeClick(data){
    this.invokeSmallCardTreeFunction.emit(data)
  }

  // onGalleryIconCloseClick(){
  //   this.invokeGalleryIconCloseFunction.emit()
  // }

  onPotreeCloseClick(){
    this.invokePotreeCloseFunction.emit()
  }

  set selected(data){
    this.activeData = data;
    this.onSharedId.emit(this.activeData)
  }

  set selectedTree(tree){
    this.activeTree = tree;
    this.onSharedTree.emit(this.activeTree)
  }

  get selectedTree() {
    return this.activeTree
  }

  set selectedFile(file){
    this.activeFile = file
    this.onSharedFile.emit(this.activeFile)
  }

  get selectedFile(){
    return this.activeFile
  }

  getData(): Observable<any> {
    return of(Data);
  }

  getDAtabyId(id) {
    return Data.find((x) => {
      return x.site_id === +id
    })
  }

  getLine(id:number): Observable<any> {
    // console.log(Line.filter((item)=>item.site_id==2));
    return of(Line.filter((item)=>item.site_id==id));
  }

  getUserInfo(data): Observable<any> {
    return of(
      User.find((user) => {
        return (
          user.username === data.username && user.password === data.password
        );
      })
    );
  }
}
